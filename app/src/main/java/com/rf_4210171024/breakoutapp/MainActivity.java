package com.rf_4210171024.breakoutapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class MainActivity extends Activity {

    BreakoutView breakoutView;

    int screenX, screenY;

    public static int maxScreenX;

    Paddle paddle;
    Ball ball;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        breakoutView = new BreakoutView(this);
        setContentView(breakoutView);
    }

    class BreakoutView extends SurfaceView implements Runnable{

        Thread gameThread = null;

        SurfaceHolder ourHolder;

        volatile boolean playing;
        boolean paused = true;

        Canvas canvas;
        Paint paint;

        Brick[] bricks = new Brick[200];
        int numBricks;

        long fps;

        private long timeThisFrame;

        public BreakoutView(Context context){
            super(context);

            ourHolder = getHolder();
            paint = new Paint();

            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            screenX = size.x;
            maxScreenX = display.getWidth();
            screenY = size.y;

            //Init Paddle
            paddle = new Paddle(screenX, screenY);
            //Init Ball
            ball = new Ball(screenX, screenY);
            //Init Brick
            numBricks = 0;

            int brickWidth = screenX / 8;
            int brickHeight = screenY / 10;

            numBricks = 0;
            for (int column = 0; column < 8; column++){

                for (int row = 0; row < 8; row++){

                    bricks[numBricks] = new Brick(row, column, brickWidth, brickHeight);
                    numBricks++;

                }

            }

            //Start Game
            createBrickAndRestart();
        }

        public void createBrickAndRestart(){
            ball.reset(screenX, screenY);
        }

        @Override
        public void run(){
            while (playing){
                long startFrameTime = System.currentTimeMillis();

                if (!paused){
                    update();
                }

                draw();

                timeThisFrame = System.currentTimeMillis() - startFrameTime;
                if (timeThisFrame >= 1){
                    fps = 1000 / timeThisFrame;
                }
            }
        }

        public void update(){
            paddle.update(fps);
            ball.update(fps);
        }

        public void draw(){
            if (ourHolder.getSurface().isValid()){
                canvas = ourHolder.lockCanvas();

                canvas.drawColor(Color.argb(255,26,128,182));
                paint.setColor(Color.argb(255,255,255,255));

                //Draw Paddle
                canvas.drawRect(paddle.getRect(), paint);

                //Draw Ball
                canvas.drawRect(ball.getRect(), paint);

                //Draw Bricks
                paint.setColor(Color.argb(255, 249, 129, 0));
                for (int i = 0; i < numBricks; i++){
                    if (bricks[i].getVisibility()){
                        canvas.drawRect(bricks[i].getRect(), paint);
                    }
                }

                //Draw HUD

                //Draw Everything
                ourHolder.unlockCanvasAndPost(canvas);
            }
        }

        public void pause(){
            playing = false;
            try {
                gameThread.join();
            }catch (InterruptedException e){
                Log.e("Error : ","joining thread");
            }
        }

        public void resume(){
            playing = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

        @Override
        public boolean onTouchEvent(MotionEvent motionEvent){

            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK){

                case MotionEvent.ACTION_DOWN :

                    paused = false;

                    if (motionEvent.getX() > screenX / 2){
                        paddle.setMovementState(paddle.RIGHT);
                    }else{
                        paddle.setMovementState(paddle.LEFT);
                    }
                    break;

                case MotionEvent.ACTION_UP:
                    paddle.setMovementState(paddle.STOPPED);
                    break;

            }
            return true;
        }

    }

    @Override
    protected void onResume(){
        super.onResume();

        breakoutView.resume();
    }

    @Override
    protected void onPause(){
        super.onPause();

        breakoutView.pause();
    }

}
