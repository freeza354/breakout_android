package com.rf_4210171024.breakoutapp;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.RectF;
import android.view.Display;

public class Paddle {

    private RectF rect;

    private float length, height;

    private float x,y;
    private int maxX;

    private float paddleSpeed;

    public final int STOPPED = 0;
    public final int LEFT = 1;
    public final int RIGHT = 2;

    private int paddleMoving = STOPPED;

    public  Paddle(int screenX, int screenY){

        length = 130;
        height = 20;

        x = screenX / 2;
        y = screenY - 20;

        maxX = screenX;

        rect = new RectF(x, y, x + length, y + height);

        paddleSpeed = 350;
    }

    public RectF getRect(){
        return rect;
    }

    public void setMovementState(int state){
        paddleMoving = state;
    }

    public void update(long fps){

        if (paddleMoving == LEFT){
            if (x <= 0) {
                paddleMoving = STOPPED;
            }
            else {
                x = x - paddleSpeed / fps;
            }
        }
        if (paddleMoving == RIGHT){
            if (x >= MainActivity.maxScreenX){
                paddleMoving = STOPPED;
            }
            else {
                x = x + paddleSpeed / fps;
            }
        }

        rect.left = x;
        rect.right = x + length;
    }

}
